//
//  MySDK.h
//  MySDK
//
//  Created by CavanSu on 2019/5/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MySDK.
FOUNDATION_EXPORT double MySDKVersionNumber;

//! Project version string for MySDK.
FOUNDATION_EXPORT const unsigned char MySDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MySDK/PublicHeader.h>


