//
//  Manager.h
//  MySDK
//
//  Created by CavanSu on 2019/5/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Manager : NSObject
+ (void)test;
@end

NS_ASSUME_NONNULL_END
