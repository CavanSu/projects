//
//  ViewController.swift
//  MyApp
//
//  Created by CavanSu on 2019/5/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import UIKit
import MySDK

class ViewController: UIViewController {
    var manager = SDKManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SDKManager.test()
        manager.startMonitorNet()
    }
}

