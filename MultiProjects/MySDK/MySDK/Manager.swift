//
//  Manager.swift
//  MySDK
//
//  Created by CavanSu on 2019/5/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import UIKit
import Alamofire

public class SDKManager: NSObject {
    private var netListener: NetworkReachabilityManager?
    
    public static func test() {
        print("Manager test")
    }
    
    public func startMonitorNet() {
        netListener = NetworkReachabilityManager(host: "www.apple.com")
        netListener?.listener = { status in
            switch status {
            case .unknown:
                print("unknown")
            case .notReachable:
                print("notReachable")
            case .reachable(let type):
                switch type {
                case .ethernetOrWiFi:
                    print("ethernetOrWiFi")
                case .wwan:
                    print("wwan")
                }
            }
        }
        
        netListener?.startListening()
    }
}
